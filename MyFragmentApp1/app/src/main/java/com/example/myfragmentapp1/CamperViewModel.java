package com.example.myfragmentapp1;

import android.content.ClipData;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

public class CamperViewModel extends ViewModel {

    private SavedStateHandle state;

    public CamperViewModel(SavedStateHandle savedStateHandle){
        state = savedStateHandle;
    }

    private final MutableLiveData<Camper> selectedCamper = new MutableLiveData<Camper>();
    public void setCamper(Camper camper) {
        selectedCamper.setValue(camper);
    }
    public LiveData<Camper> getSelectedCamper() {
        return selectedCamper;
    }


}
