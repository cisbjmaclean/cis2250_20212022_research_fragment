package com.example.myfragmentapp1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentRed#newInstance} factory method to
 * create an instance of this fragment.
 */
public class
FragmentRed extends Fragment {

    private CamperViewModel camperModel;
    private Button button;
    private int counter = 0;
    private Camper camper;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentRed() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ragmentRed.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRed newInstance(String param1, String param2) {
        FragmentRed fragment = new FragmentRed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentRed newInstance() {
        FragmentRed fragment = new FragmentRed();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_red, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //BJM Note it is the requireActivity which ensures view model shared with the activity
        camperModel = new ViewModelProvider(requireActivity()).get(CamperViewModel.class);

        if(camper == null){
            camper = new Camper();
            camper.setFirstName("Joe");
            camper.setLastName("Smith");
            Log.d("BJM updating camper", "Updating the camper in the view model");
            camperModel.setCamper(camper);
        }

        button = view.findViewById(R.id.buttonOnRedFragment);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;
                camper.setLastName(camper.getLastName()+counter);
                Log.d("BJM in onClick","Changing the camper name");
                camperModel.setCamper(camper);
            }
        });




    }
}